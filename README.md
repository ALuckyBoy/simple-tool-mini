# demo-uni

## Project setup
```
npm install 
cnpm install
yarn
```

### Compiles and hot-reloads for development
```
npm run serve 
npm run dev:mp-weixin 
cnpm run serve 
cnpm run dev:mp-weixin 
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
## Other
### My Blog
```
https://blog.csdn.net/weixin_43797577
http://mcongblog.com/
```
### personal website
```
http://mcongblog.com/ // 个人博客
https://wmc0216.com/ // 小兔鲜
```
### Project Address
```
https://gitee.com/ALuckyBoy // 微信搜索小程序：阿叨工具箱
```