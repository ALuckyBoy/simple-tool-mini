import Vue from 'vue'
import App from './App'
// 导入 request.js 模块
import http from '@/utils/request'
// 挂载到 Vue.prototype
Vue.prototype.$http = http
// 导入 message.js 模块
import { showMessage } from '@/utils/message'

Vue.prototype.$msg = showMessage
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
