/**
 * 公司编码列表
 */
export const  expressageCodeList = [
  {
    cpCode: 'SF',
    expressCompany: '顺丰快递',
  },
  {
    cpCode: 'YTO',
    expressCompany: '圆通快递',
  },
  {
    cpCode: 'ZTO',
    expressCompany: '中通快递',
  },
  {
    cpCode: 'YUNDA',
    expressCompany: '韵达快递',
  },
  {
    cpCode: 'YDKY',
    expressCompany: '韵达快运',
  },
  {
    cpCode: 'STO',
    expressCompany: '申通快递',
  },
  {
    cpCode: 'TTKDEX',
    expressCompany: '天天快递',
  },
  {
    cpCode: 'JD',
    expressCompany: '京东快递',
  },
  {
    cpCode: 'EMS',
    expressCompany: '	中国邮政（其他常用名称：邮政国内小包、EMS、邮政标准快递、E邮宝、EMS经济快递、EMS特快专递、邮政快递包裹、EMS快递包裹、邮政EMS速递、邮政平邮、邮政商务小包、平邮、邮包、邮政快递，都可以使用此对照关系查询或订阅物流）',
  },
  {
    cpCode: 'DBKD',
    expressCompany: '德邦快递',
  },
  {
    cpCode: 'DBL',
    expressCompany: '德邦物流',
  },
  {
    cpCode: 'DBKY',
    expressCompany: '德邦快运',
  },
  {
    cpCode: 'SURE',
    expressCompany: '速尔快运',
  },
  {
    cpCode: 'UC',
    expressCompany: '优速快递',
  },
  {
    cpCode: 'JT',
    expressCompany: '极兔速递',
  },
  {
    cpCode: 'KYE',
    expressCompany: '跨越速运',
  },
  {
    cpCode: 'CP471906',
    expressCompany: '顺心捷达',
  },
  {
    cpCode: 'ANE56',
    expressCompany: '安能物流',
  },
  {
    cpCode: 'BESTQJT',
    expressCompany: '百世快运',
  },
  {
    cpCode: 'FW',
    expressCompany: '丰网速递',
  },
  {
    cpCode: 'YMDD',
    expressCompany: '壹米滴答',
  },
  {
    cpCode: 'JYM',
    expressCompany: '加运美',
  },
  {
    cpCode: 'ZTOKY',
    expressCompany: '中通快运',
  },
  {
    cpCode: 'CP449455',
    expressCompany: '京广速递',
  },
  {
    cpCode: 'ZJS',
    expressCompany: '宅急送',
  },
  {
    cpCode: 'FAST',
    expressCompany: '快捷快递',
  },
  {
    cpCode: 'FEDEX',
    expressCompany: '联邦快递',
  },
  {
    cpCode: 'RRS',
    expressCompany: '日日顺',
  },
  {
    cpCode: 'SNWL',
    expressCompany: '苏宁快递',
  },
  {
    cpCode: 'ZMKM',
    expressCompany: '丹鸟',
  },
  {
    cpCode: 'ZMKMKD',
    expressCompany: '丹鸟快递',
  },
  {
    cpCode: 'RFD',
    expressCompany: '如风达',
  },
  {
    cpCode: 'YCKY',
    expressCompany: '远成快运',
  },
  {
    cpCode: 'JYXP',
    expressCompany: '九曳供应链',
  },
  {
    cpCode: 'CP468398',
    expressCompany: '圆通承诺达',
  },
  {
    cpCode: 'RRS',
    expressCompany: '日日顺',
  },
  {
    cpCode: 'RRS',
    expressCompany: '日日顺',
  },
  {
    cpCode: 'GTO',
    expressCompany: '国通快递',
  },
  {
    cpCode: 'QFKD',
    expressCompany: '全峰快递',
  },
  {
    cpCode: 'SDSD',
    expressCompany: '山东递速',
  },
  {
    cpCode: 'XFWL',
    expressCompany: '信丰物流',
  },
  {
    cpCode: 'SDSD',
    expressCompany: '百世云配',
  },
  {
    cpCode: 'LE10032270',
    expressCompany: '韵达同城',
  },
  {
    cpCode: 'PADTF',
    expressCompany: '平安达腾飞',
  },
  {
    cpCode: 'HOAU',
    expressCompany: '天地华宇',
  },
  {
    cpCode: 'STOKY',
    expressCompany: '申通快运',
  },
  {
    cpCode: 'ZTKY',
    expressCompany: '中铁物流/飞豹快递',
  },
  {
    cpCode: 'EWINSHINE',
    expressCompany: '万象物流',
  },
  {
    cpCode: 'QRT',
    expressCompany: '全日通',
  },
  {
    cpCode: 'GZFY',
    expressCompany: '凡宇快递',
  },
  {
    cpCode: 'XBWL',
    expressCompany: '新邦物流',
  },
  {
    cpCode: 'CRE',
    expressCompany: '中铁快运',
  },
  {
    cpCode: 'LTS',
    expressCompany: '联昊通',
  },
  {
    cpCode: 'SHENGHUI',
    expressCompany: '盛辉物流',
  },
  {
    cpCode: 'HUANGMAJIA',
    expressCompany: '黄马甲配送',
  },
  {
    cpCode: 'CHENGBANG',
    expressCompany: '晟邦物流',
  },
  {
    cpCode: 'GZLT',
    expressCompany: '飞远配送',
  },
  {
    cpCode: 'HZABC',
    expressCompany: '飞远(爱彼西)配送',
  },
  {
    cpCode: 'YCT',
    expressCompany: '黑猫宅急便',
  },
  {
    cpCode: 'COE',
    expressCompany: 'COE',
  },
  {
    cpCode: 'DTW',
    expressCompany: '大田物流',
  },
  {
    cpCode: 'AIR',
    expressCompany: '亚风速递',
  },
  {
    cpCode: 'WJ',
    expressCompany: '万家物流',
  },
  {
    cpCode: 'D4PX',
    expressCompany: '递四方',
  },
  {
    cpCode: 'UPS',
    expressCompany: 'UPS',
  },
  {
    cpCode: 'SUIJIAWL',
    expressCompany: '穗佳物流',
  },
  {
    cpCode: 'SUTENG',
    expressCompany: '速腾快递',
  },
  {
    cpCode: 'ANNTO',
    expressCompany: '安得智联',
  },
  {
    cpCode: 'DISU',
    expressCompany: 'D速',
  },
  {
    cpCode: 'ZZJHTD',
    expressCompany: '郑州建华',
  },
  {
    cpCode: 'ZTOGJ',
    expressCompany: '中通国际',
  },
  {
    cpCode: 'SXJH',
    expressCompany: '建华快递',
  },
  {
    cpCode: 'ZL',
    expressCompany: '四川增联供应链',
  },
  {
    cpCode: 'JUSDA',
    expressCompany: '准时达',
  },
  {
    cpCode: 'YDGJ',
    expressCompany: '韵达国际',
  },
  {
    cpCode: 'FENGNIAO',
    expressCompany: '蜂鸟配送',
  },
  {
    cpCode: 'FJGJ',
    expressCompany: '泛捷国际物流',
  },
  {
    cpCode: 'CVP',
    expressCompany: '潮优配',
  },
  {
    cpCode: 'LSWL',
    expressCompany: '林氏物流',
  },
  {
    cpCode: 'CQLY',
    expressCompany: '重庆铃宇消费品供应链',
  },
  {
    cpCode: 'XINZHIHUI',
    expressCompany: '新智慧',
  },
  {
    cpCode: 'STOINTL',
    expressCompany: '申通国际',
  },
  {
    cpCode: 'HBZL',
    expressCompany: '湖北众联',
  },
  {
    cpCode: 'GOODKUAIDI',
    expressCompany: 'GOOD快递',
  },
  {
    cpCode: 'EWE_GLOBAL',
    expressCompany: 'EWE GLOBAL',
  },
  {
    cpCode: 'WDSWL',
    expressCompany: '鸿达顺物流',
  },
  {
    cpCode: 'AXD',
    expressCompany: '安鲜达',
  },
  {
    cpCode: 'GDNFCMFXWL',
    expressCompany: '广东南方传媒发行物流',
  },
  {
    cpCode: 'XLOBO',
    expressCompany: '贝海国际速递',
  },
  {
    cpCode: 'ESDEX',
    expressCompany: '卓志速运',
  },
  {
    cpCode: 'YTOGJ',
    expressCompany: '圆通国际',
  },
  {
    cpCode: 'STWL',
    expressCompany: '速通物流',
  },
  {
    cpCode: 'WAJ',
    expressCompany: '沃埃家',
  },
  {
    cpCode: 'UBON',
    expressCompany: '优邦速运',
  },
  {
    cpCode: 'WND',
    expressCompany: 'WnDirect',
  },
  {
    cpCode: 'SXXF',
    expressCompany: '山西馨富',
  },
  {
    cpCode: 'YC',
    expressCompany: '远长物流',
  },
  {
    cpCode: 'BSGJ',
    expressCompany: '百世国际',
  },
  {
    cpCode: 'YUD',
    expressCompany: '长发快递',
  },
  {
    cpCode: 'KMYRX',
    expressCompany: '昆明云瑞祥',
  },
  {
    cpCode: 'DHL',
    expressCompany: 'DHL全球',
  },
  {
    cpCode: 'CN7000001011758',
    expressCompany: '泰进物流',
  },
  {
    cpCode: 'BYZH',
    expressCompany: '贝业智慧物流',
  },
  {
    cpCode: 'JQWL',
    expressCompany: '浙江鉴强物流',
  },
  {
    cpCode: 'ESB',
    expressCompany: 'E速宝',
  },
  {
    cpCode: 'SZXK',
    expressCompany: '苏州熙康',
  },
  {
    cpCode: 'DDS',
    expressCompany: '点点送',
  },
  {
    cpCode: 'SUYUN123',
    expressCompany: '58速运',
  },
  {
    cpCode: 'SEND2CHINA',
    expressCompany: 'Send2China',
  },
  {
    cpCode: 'CYEXP',
    expressCompany: '长宇快递',
  },
  {
    cpCode: 'TTKEU',
    expressCompany: '天天欧洲物流',
  },
  {
    cpCode: 'SQWL',
    expressCompany: '商桥物流',
  },
  {
    cpCode: 'WORLDCPS',
    expressCompany: '天马物流',
  },
  {
    cpCode: 'KXTX',
    expressCompany: '卡行天下',
  },
  {
    cpCode: 'AOYOUZGKY',
    expressCompany: '澳邮中国快运',
  },
  {
    cpCode: 'EFS',
    expressCompany: 'EFSPOST',
  },
  {
    cpCode: 'AXWL',
    expressCompany: '安迅物流',
  },
  {
    cpCode: 'RUSTON',
    expressCompany: '俄速通',
  },
  {
    cpCode: 'PAKD',
    expressCompany: '平安快递',
  },
  {
    cpCode: 'TDWL_123456',
    expressCompany: '腾达物流',
  },
  {
    cpCode: 'XZB',
    expressCompany: '鑫泽邦物流',
  },
  {
    cpCode: 'HCT',
    expressCompany: '新竹物流',
  },
  {
    cpCode: 'MGSD',
    expressCompany: '美国速递',
  },
  {
    cpCode: 'SCJXWLYXGS',
    expressCompany: '余氏东风',
  },
  {
    cpCode: 'DD',
    expressCompany: '当当宅配',
  },
  {
    cpCode: 'AST',
    expressCompany: '安世通',
  },
  {
    cpCode: 'FTDKD',
    expressCompany: '富腾达快递',
  },
  {
    cpCode: 'EHAOYAO',
    expressCompany: '好药师物流',
  },
  {
    cpCode: 'RBYZEMS',
    expressCompany: '日本邮政EMS',
  },
  {
    cpCode: 'CGKD',
    expressCompany: '程光快递',
  },
  {
    cpCode: 'DOD',
    expressCompany: '门对门',
  },
  {
    cpCode: 'YDTKD',
    expressCompany: '易达通快递',
  },
  {
    cpCode: 'BHWL',
    expressCompany: '保宏物流',
  },
  {
    cpCode: 'XAJ',
    expressCompany: '新安居仓配',
  },
  {
    cpCode: 'CSZX',
    expressCompany: '城市之星',
  },
  {
    cpCode: 'XYWL',
    expressCompany: '秀驿物流',
  },
  {
    cpCode: 'LTJZPS',
    expressCompany: '蓝豚家装配送',
  },
  {
    cpCode: 'DJ56',
    expressCompany: '东骏快捷',
  },
  {
    cpCode: 'NEDA',
    expressCompany: '能达速递',
  },
  {
    cpCode: 'CHS',
    expressCompany: '重庆中环',
  },
  {
    cpCode: 'YXWLJT',
    expressCompany: '宇鑫物流',
  },

]