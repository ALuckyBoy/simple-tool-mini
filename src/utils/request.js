import FlyIO from 'flyio/dist/npm/wx'

// 创建新的 FlyIO 实例
const http = new FlyIO()

http.config.timeout = 30000 // 设置超时(服务器性能偏差)
http.config.baseURL = 'https://api.vvhan.com/api' // 设置基地址
// 设置请求头
http.config.headers = {
  'X-APISpace-Token': 'aug5rcbg3vgyusnreb3qavhd1t3ewevb',
  'Authorization-Type': 'apikey',
  'Content-Type': 'application/json'
}
// 请求拦截器
http.interceptors.request.use((request) => {
  uni.showLoading({
    title: '加载中...',
    mask: true
  })
  return request
})

// 响应拦截器
http.interceptors.response.use(
  (response) => {
    // 显示加载框
    uni.hideLoading()

    // 只取返回的数据字段
    return response.data
  },
  (err) => {
    uni.hideLoading()
    return Promise.reject(err)
  }
)

// 导出
export default http
