/**
 * 封装Total组件
 * @param {*} title 提示信息 默认为请求数据失败
 * @param {*} duration 显示时间 默认为2000ms
 * @param {*} icon icon图标 默认为none
 */
export const showMessage = (
  title = '请求数据失败',
  duration = 2000,
  icon = 'none'
) => {
  uni.showToast({
    title,
    duration,
    icon
  })
}
