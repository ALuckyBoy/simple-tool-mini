/**
 * 混入分享好友&&分享至朋友圈
 */
export default {
  // 分享给好友
  onShareAppMessage() {
    const promise = new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          title: '阿叨工具箱'
        })
      }, 2000)
    })
    return {
      title: '阿叨工具箱',
      path: '/pages/home/index',
      promise
    }
  },
  // 分享到朋友圈
  onShareTimeline() {
    return {
      title: '阿叨工具箱'
    }
  }
}